package common;

import java.util.Observable;

public abstract class SmartObservable extends Observable {
	/**
	 * Convenience method for Observables
	 * @param args
	 */
	protected void notifyChanges(Object args) {
		setChanged();
		notifyObservers(args);
	}
}
