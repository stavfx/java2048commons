package data;

public class Game2048HelpRequest {
	private int[][] state;
	private int score;

	public Game2048HelpRequest(int[][] state, int score) {
		this.state = state;
		this.score = score;
	}

	public int[][] getState() {
		return state;
	}

	public void setState(int[][] state) {
		this.state = state;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

}
